from cryptography.fernet import Fernet
import json
import os
import sys
import getopt
import argparse

class Locker(object):
    def __init__(self, key, filepath):
        self.key = key
        self.filepath = filepath
        self.content = {}
        self.fernet = Fernet(key)

        if os.path.exists(filepath):
            self._loadFile()

    def setCubby(self, key, value):
        self.content[key] = value

    def _saveFile(self):
        with open(self.filepath, 'wb') as locker:
            contentString = bytes(json.dumps(self.content), 'utf-8')
            encryptedContentString = self.fernet.encrypt(contentString)
            locker.write(encryptedContentString)
    
    def _loadFile(self):
        with open(self.filepath, 'rb') as locker:
            content = locker.read()
            unencryptedContentString = self.fernet.decrypt(content)
            self.content = json.loads(unencryptedContentString)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--version", help="show locker version", action="store_true")
    parser.add_argument("-k", "--key", help="raw locker key", action="store")
    parser.add_argument("-d", "--directory", help="Location of the locker file", action="store", required=True)
    parser.add_argument("-c", "--create", help="Create a new locker", action="store_true")
    args = parser.parse_args()

    key = None
    creating = False
    filepath = args.directory

    if not (args.create or args.key):
        raise Exception("You must provide a key (-k:), or you can create one (-c)")

    
    if(args.version):
        print("0.0.0")
        sys.exit(2)

    if(args.create):
        key = Fernet.generate_key()
        print("\n\nYour key is:\n{}\n\n".format(key))

    print("Opening locker at {}".format(filepath))
    locker = Locker(key, filepath)

    #Create a locker
        #Return Key
    #Read the contents of a locker
        #Set Key
        #Display
    #Set a cubby for a locker
        #Set Key
        #Set Cubby Name
        #Set Cubby Value
    



