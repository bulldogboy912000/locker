import unittest
import os
import calendar
import json
import copy
from locker import Locker
from cryptography.fernet import Fernet

LOCKER_PATH = ".locker_test"
KEY = Fernet.generate_key()
GARBAGE = "ADSFSDFASFD"
CUBBY_NAME = "cubby"
CUBBY_VALUE = "cubby value"

class TestLocker(unittest.TestCase):

    def setUp(self):
        self.locker = Locker(KEY, LOCKER_PATH)
        self.locker.setCubby(CUBBY_NAME, GARBAGE)
        self.locker._saveFile()

    def tearDown(self):
        if os.path.exists(LOCKER_PATH):
            os.remove(LOCKER_PATH)

    def test_locker_exists(self):
        self.assertIsNotNone(self.locker)

    def test_set_key(self):
        self.locker.setCubby(CUBBY_NAME, CUBBY_VALUE)
        self.assertEqual(self.locker.content[CUBBY_NAME], CUBBY_VALUE)

    def test_saves_encrypted_file(self):
        with open(LOCKER_PATH) as encryptedLocker:
            encryptedData = encryptedLocker.read()
            unencryptedString = json.dumps(self.locker.content)
            self.assertIsNotNone(encryptedData)
            self.assertNotEqual(encryptedData, unencryptedString)

    def test_loads_encrypted_file(self):
        beforeContent = copy.deepcopy(self.locker.content)
        self.locker._loadFile()
        afterContent = self.locker.content
        self.assertEqual(beforeContent, afterContent)

    def test_independent_encrypt_decrypt(self):
        locker1 = Locker(KEY, LOCKER_PATH)

        locker1.setCubby("mysecret", "Was returned correctly")
        locker1._saveFile()

        locker2 = Locker(KEY, LOCKER_PATH)
        self.assertEqual(locker2.content["mysecret"], "Was returned correctly")

        
if __name__ == '__main__':
    unittest.main()